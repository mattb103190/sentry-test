import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sen-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'sentry-test';

  constructor() {}

  ngOnInit(): void {
    this.simulateError();
  }

  simulateError(): void {
    throw new Error('Sentry Test');
  }
}
