import { Controller, Get, InternalServerErrorException, UseInterceptors } from '@nestjs/common';
import { AppService } from './app.service';
import { SentryInterceptor } from './sentry-interceptor';

@UseInterceptors(SentryInterceptor)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  throwError() {
    throw new InternalServerErrorException('Sentry Test');
  }
}
