import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as Sentry from '@sentry/node';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  Sentry.init({
    dsn:
      'https://6420356963044e19a84bc002ae8a5c4d@o540251.ingest.sentry.io/5658347',
  });
  await app.listen(3000);
}
bootstrap();
